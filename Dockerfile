FROM ubuntu:16.04

RUN apt-get update && \
  apt-get install -y nginx && \
  apt-get install -y nano && \
   apt-get install -y htop && \
  cd /opt && \
  mkdir ActiveWebsites && \
  cd ActiveWebsites && \
  mkdir data-mining && \
  cd data-mining


RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

COPY config/nginx /etc/nginx/sites-available

COPY dist/ /opt/ActiveWebsites/data-mining
RUN chown -R www-data:www-data /opt/ActiveWebsites/data-mining

COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod 777 docker-entrypoint.sh
CMD ./docker-entrypoint.sh

EXPOSE 80
EXPOSE 443
RUN mkdir /etc/nginx/logs









