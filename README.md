# Build Code
This will output compiled code in /dist. This is used prior to updating a docker image.
```
ng build --prod
```


# Running Docker 
Change the URLS accordingly
```
docker build -t data-mining .
docker run -d -p 8005:80 -e HEATMAP_URL=http://www.mocky.io/v2/5a5e19f233000022222319235b -e BUSINESS_URL_B=http://198.211.125.71:5558 -e BUSINESS_URL_A=http://198.211.125.71:5559 data-mining
```

# Running the Frontend without Docker (Dev purposes)

1. Change the settings in /config/app.dev.ts
2. Run the following command
```
npm start
```
3. Check http://localhost:4200
