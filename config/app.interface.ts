export interface DIConfig {
  mapPointsUrl: string;
  getBusinessListUrl: string;
  getSimilarBusinesses: string;
  getBusinessDetails: string;
  getBusinessCompareUrl: string;
}
