import { DIConfig } from './app.interface';

export const prodConfig: DIConfig = {
  mapPointsUrl: 'http://198.211.125.71:5559/location?longitude=%lng%&latitude=%lat%&radius=%radius%',
  getBusinessListUrl: 'http://198.211.125.71:5559/business/simple',
  getSimilarBusinesses: 'http://198.211.125.71:5558/getSimilarBusiness?b_id=%bid%',
  getBusinessDetails: 'http://198.211.125.71:5559/business/id/%bid%',
  getBusinessCompareUrl: 'http://198.211.125.71:5558/compareBusinesses?b_id1=%bidA%&b_id2=%bidB%'
};
