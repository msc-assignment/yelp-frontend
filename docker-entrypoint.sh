#!/usr/bin/env bash

#Remove the default Nginx Configuration
rm -rf /etc/nginx/sites-enabled/default

echo "Replacing ENV Variables in JS files" > /var/log/env.log
cd /opt/ActiveWebsites/data-mining

unset IFS
args=() i=0
for var in $(compgen -e); do
    echo "$var=${!var}" >> /var/log/env.log
    find . -type f -exec sed -i "s@\[\[$var\]\]@${!var}@g" '{}' \;
done
echo "Done" >> /var/log/env.log

#Replace NGINX Conf File
ln -s /etc/nginx/sites-available/nginx.conf /etc/nginx/sites-enabled/nginx.conf

service nginx start
tail -f /var/log/nginx/{access.log,error.log}

