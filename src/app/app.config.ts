import { InjectionToken } from '@angular/core';
import { devConfig } from '../../config/app.dev';
import { DIConfig } from '../../config/app.interface';
import { prodConfig } from '../../config/app.prod';

import { environment } from '../environments/environment';

export const appConfigFactory = () => {
  if (!environment.production) {
    return devConfig;
  }

  return prodConfig;
};

export let AppConfig: InjectionToken<DIConfig> = new InjectionToken('app.config');
