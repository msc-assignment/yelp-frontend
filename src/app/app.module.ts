import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MapComponent} from './map/map.component';
import {RouterModule, Routes} from '@angular/router';
import {HomepageComponent} from './homepage/homepage.component';
import {GeneralDataService} from './services/general-data.service';
import {HttpModule} from '@angular/http';
import {MatAutocompleteModule, MatButtonModule, MatChipsModule, MatTabsModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SimilarlistComponent} from './homepage/similarlist/similarlist.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SearchComponent} from './homepage/search/search.component';
import {AppConfig, appConfigFactory} from "./app.config";
import {CompareComponent} from './compare/compare.component';


const appRoutes: Routes = [
  {
    path: 'home',
    component: HomepageComponent,
  },
  {
    path: 'business/:id',
    component: SimilarlistComponent,
  },
  {
    path: 'compare',
    component: CompareComponent
  },
  {
    path: 'map',
    component: MapComponent
  },
  {
    path: '', redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: HomepageComponent
  },
];

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    HomepageComponent,
    SimilarlistComponent,
    SearchComponent,
    CompareComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatChipsModule,
    FormsModule,
    MatTabsModule,
    MatAutocompleteModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    MatButtonModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatChipsModule
  ],
  providers: [
    GeneralDataService,
    {provide: AppConfig, useFactory: appConfigFactory}
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
