import {Component, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import {GeneralDataService} from "../services/general-data.service";
import {Router} from "@angular/router";
import {map, startWith} from "rxjs/operators";

@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.css']
})
export class CompareComponent implements OnInit {


  public businessControlFormA: FormControl;
  public businessControlFormB: FormControl;
  public businessA;
  public businessB;
  public filteredBusinessA: Observable<any[]>;
  public filteredBusinessB: Observable<any[]>;
  public similarPercentage;

  public businessList = [];

  constructor(public generalService: GeneralDataService, public router: Router) {

    this.businessControlFormA = new FormControl();
    this.businessControlFormB = new FormControl();

    this.filteredBusinessA = this.businessControlFormA.valueChanges
      .pipe(
        startWith(''),
        map(item => item ? this.filterBusiness(item) : this.businessList.slice())
      );
    this.filteredBusinessB = this.businessControlFormB.valueChanges
      .pipe(
        startWith(''),
        map(item => item ? this.filterBusiness(item) : this.businessList.slice())
      );
  }


  filterBusiness(name: string) {
    return this.businessList.filter(item =>
      item.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }


  ngOnInit() {
    this.loadInitialData();
  }

  loadInitialData() {
    this.generalService.getBusinessList().subscribe(
      (data) => {
        this.businessList = data;
      },
      (error) => {
        console.error('There was an error while loading initial data!');
      });
  }

  onSelectionChangeA(event) {
    //filter the data with name to get the object selected
    this.businessA = this.businessList.find(b => b.name === event);

  }

  onSelectionChangeB(event) {
    //filter the data with name to get the object selected
    this.businessB = this.businessList.find(b => b.name === event);
  }

  compareFormSubmit() {
    this.generalService.compareBusiness(this.businessA.id, this.businessB.id).subscribe(
      (data) => {
        this.businessCompareDataLoaded(data);
      },
      (error) => {
        console.error('There was an error while comparing businesses!');
      });
    return false;
  }

  businessCompareDataLoaded(data) {
    if (data && data['similarity']) {
      this.similarPercentage = (data['similarity'] * 100).toFixed(2);
    }
  }
}
