import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';

import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {Router} from "@angular/router";
import {GeneralDataService} from "../../services/general-data.service";


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  businessControlForm: FormControl;
  filteredBusiness: Observable<any[]>;
  businessList = [];

  constructor(public generalService: GeneralDataService, public router: Router) {

    this.businessControlForm = new FormControl();
    this.filteredBusiness = this.businessControlForm.valueChanges
      .pipe(
        startWith(''),
        map(item => item ? this.filterBusiness(item) : this.businessList.slice())
      );
  }


  filterBusiness(name: string) {
    return this.businessList.filter(item =>
      item.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }


  ngOnInit() {
    this.loadInitialData();
  }

  loadInitialData() {
    this.generalService.getBusinessList().subscribe(
      (data) => {
        this.businessList = data;
      },
      (error) => {
        console.error('There was an error while loading initial data!');
      });
  }

  onSelectionChange(event) {
    //filter the data with name to get the object selected
    const selected = this.businessList.find(b => b.name === event);
    this.router.navigateByUrl('/business/' + selected.id);
  }

}
