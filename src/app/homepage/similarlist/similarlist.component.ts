import {Component, AfterViewInit, ViewChild, Input, OnInit} from '@angular/core';
import {GeneralDataService} from "../../services/general-data.service";
import {ActivatedRoute, Router} from "@angular/router";
declare var $;

declare var google;

@Component({
  selector: 'app-similarlist',
  templateUrl: './similarlist.component.html',
  styleUrls: ['./similarlist.component.css']
})
export class SimilarlistComponent implements AfterViewInit, OnInit {
  @Input() results;

  public categories;
  public similarBusinesses;
  public businessData;
  public map;
  public selectedIndex;

  constructor(public generalService: GeneralDataService, public route: ActivatedRoute, public router: Router) {
  }

  renderMap() {
    if(!this.map) {
      this.map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
      });
    }
    var coord = new google.maps.LatLng(this.businessData.latitude, this.businessData.longitude);

    this.map.setCenter(coord);

    var marker = new google.maps.Marker({
      position: coord,
      title: this.businessData.name
    });

    marker.setMap(this.map);
  }

  ngAfterViewInit() {
    //do nothing
  }

  initData(businessId) {
    this.loadSimilarBusiness(businessId);
    this.loadBusinessDetails(businessId);
  }


  loadSimilarBusiness(businessId) {
    this.generalService.getSimilarBusiness(businessId).subscribe(
      (data) => {
        this.similarDataLoaded(data);
      },
      (error) => {
        console.error('There was an error while loading similar businesses!');
      });

  }

  similarDataLoaded(data) {
    for (var bName in data) {
      if (data.hasOwnProperty(bName)) {
        if (data[bName]) {
          this.categories = data[bName]['categories'];
          this.processSimilarResults(data[bName]['similar']);
        }
      }
    }

  }

  ngOnInit() {
    if (!this.results) {
      this.route.params.subscribe(params => {
        this.initData(params.id);
      });
    } else {
      this.initData(this.results.id);
    }
  }

  loadBusinessDetails(businessId) {
    this.generalService.getBusinessDetails(businessId).subscribe(
      (data) => {
        this.businessDataLoaded(data);
      },
      (error) => {
        console.error('There was an error while loading business data!');
      });
  }

  businessDataLoaded(data) {
    this.businessData = data;
    this.renderMap();
  }

  processSimilarResults(similarData) {
    if (similarData) {
      this.similarBusinesses = [];
      for (var similarBusinessName in similarData) {
        if (similarData.hasOwnProperty(similarBusinessName)) {
          if (similarData[similarBusinessName]) {
            let objRow: BusinessRow = {
              name: similarBusinessName,
              categories: similarData[similarBusinessName]['categories'],
              similarity: (similarData[similarBusinessName]['similarity'] * 100).toFixed(2) + '%',
              id: similarData[similarBusinessName]['id'],
            };
            this.similarBusinesses.push(objRow);
          }
        }
      }
    }
  }

  reset() {
    $( "#item-container" ).fadeOut('fast').delay(1000).fadeIn('slow');
    this.businessData = null;
    this.categories = null;
    this.similarBusinesses = null;
    this.selectedIndex = 0;

  }

  redirectToPage(id) {
    this.reset();
    this.router.navigateByUrl('/business/' + id);
    return false;
  }

}

export interface BusinessRow {
  name: string;
  categories: string;
  similarity: string;
  id: string;
}


