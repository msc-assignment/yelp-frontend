import {Component, OnInit} from '@angular/core';
import {GeneralDataService} from "../services/general-data.service";
import {Observable} from "rxjs/Observable";

declare var google;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  public map;
  public points = [];
  public currentRadius = 0;
  public mapCenter;
  public icons;
  public defaultLat = '36.185105';
  public defaultLng = '-115.225161';
  public markers = [];


  constructor(public generalService: GeneralDataService) {
    this.icons = {
      negative: {
        icon: 'https://image.ibb.co/hDHgrw/star_2.png'
      },
      positive: {
        icon: 'https://image.ibb.co/gsAUdb/star_3.png'
      },
      neutral: {
        icon: 'https://image.ibb.co/ebDXyb/star_1.png'
      }
    };
  }

  ngOnInit() {
    this.initMap();
    this.checkRadius();


  }

  initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12,
      center: new google.maps.LatLng(this.defaultLat, this.defaultLng)
    });

    this.map = map;


    Observable.interval(1000).subscribe(x => {
      if (this.checkCenterChanged()) {
        this.checkRadius();
        this.clearMarkers();
        this.renderPoints(this.mapCenter.lat(), this.mapCenter.lng(), this.currentRadius);
      }
    });
  }


  renderPoints(lat, lng, radius) {
    return this.generalService.getMapPoints(lat, lng, radius).subscribe(
      data => this.pointsLoaded(data),
      err => {
        alert('Something went wrong!');
      }
    );
  }

  clearMarkers() {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers = [];
  }

  pointsLoaded(data) {

    if (data) {
      for (let i = 0; i < data.length; i++) {
        var pinType;
        if (data[i]['countNegative'] === data[i]['countNeutral']) {
          pinType = 'negative';
        } else if (data[i]['countPositive'] === data[i]['countNeutral']) {
          pinType = 'positive';
        } else if (data[i]['countPositive'] > data[i]['countNegative']) {
          pinType = 'positive';
        } else if (data[i]['countNegative'] > data[i]['countPositive']) {
          pinType = 'negative';
        } else if (data[i]['countPositive'] === data[i]['countNegative']) {
          pinType = 'neutral';
        } else {
          pinType = 'neutral';
        }

        // Create infoWindow
        var infoWindow = new google.maps.InfoWindow({
          content: 'Content goes here..'
        });

        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(data[i]['latitude'], data[i]['longitude']),
          icon: this.icons[pinType].icon,
          title: data[i]['name'],
          map: this.map
        });


        this.markers.push(marker);


      }
    }


  }

  checkCenterChanged() {
    if (this.map) {
      var tmpCenter = this.map.getCenter();
      //For first iteration
      if (!this.mapCenter) {
        this.mapCenter = tmpCenter;
        return true;
      }

      if (tmpCenter.lat() != this.mapCenter.lat() || tmpCenter.lng() != this.mapCenter.lng()) {
        this.mapCenter = tmpCenter;
        return true;
      } else {
        return false;
      }
    }


  }

  checkRadius() {

    var bounds = this.map.getBounds();
    if (bounds) {
      var center = bounds.getCenter();
      var ne = bounds.getNorthEast();

      // r = radius of the earth in statute miles
      var r = 3963.0;

      // Convert lat or lng from decimal degrees into radians (divide by 57.2958)
      var lat1 = center.lat() / 57.2958;
      var lon1 = center.lng() / 57.2958;
      var lat2 = ne.lat() / 57.2958;
      var lon2 = ne.lng() / 57.2958;

      // distance = circle radius from center to Northeast corner of bounds
      var dis = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) +
        Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));
      this.currentRadius = dis;
    }
  }


}
