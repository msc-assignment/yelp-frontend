import {Inject, Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import {AppConfig} from '../app.config';
import {DIConfig} from '../../../config/app.interface';

@Injectable()
export class GeneralDataService {

  constructor(public http: Http, @Inject(AppConfig) private config: DIConfig) { }

  getMapPoints(lat, lng, radius) {
    radius = radius.toFixed(0);
    console.log('Retrieving for ' + lat + ',' + lng + ' for radius of '+ radius);
    let url: string = this.config.mapPointsUrl;
    url = url.replace('%radius%', radius);
    url = url.replace('%lng%', lng);
    url = url.replace('%lat%', lat);
    return this.http.get(url).map(response => response.json());

  }

  getBusinessList() {
    let url: string = this.config.getBusinessListUrl;
    return this.http.get(url).map(response => response.json());
  }

  getSimilarBusiness(businessId) {
    let url: string = this.config.getSimilarBusinesses;
    url = url.replace('%bid%', businessId);
    return this.http.get(url).map(response => response.json());
  }

  getBusinessDetails(businessId) {
    let url: string = this.config.getBusinessDetails;
    url = url.replace('%bid%', businessId);
    return this.http.get(url).map(response => response.json());
  }

  compareBusiness(businessAId, businessBId) {
    let url: string = this.config.getBusinessCompareUrl;
    url = url.replace('%bidA%', businessAId);
    url = url.replace('%bidB%', businessBId);

    return this.http.get(url).map(response => response.json());
  }
}
